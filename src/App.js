import React from 'react';
import data from "./data.json"
import PropTypes from 'prop-types';
import { makeStyles, createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import SvgIcon from '@material-ui/core/SvgIcon';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import useMediaQuery from '@material-ui/core/useMediaQuery';

var rows = data;
var filterList = [];
const propertyNames = Object.keys(rows[0]);
const name = [propertyNames.shift(), propertyNames.shift()];


const theme = createMuiTheme({
  typography: {
    fontFamily: 'inter',
    fontSize: 16,
    fontStyle: 'normal',
    alignItems: 'center',
    display: 'flex',
    fontWeightMedium: 400,
    fontWeightBold: 700,
    h1: {//heading2
      fontSize: 32,
      lineHeight: "39px",
      fontWeight: 'bold',
    },
    h2: {//heading3
      fontSize: 26,
      lineHeight: "31px",
      fontWeight: 'bold',
    },
    h3: {//heading4
      fontSize: 22,
      lineHeight: "27px",
      fontWeight: 'bold',
    },
    h4: {//heading5
      fontSize: 18,
      lineHeight: "22px",
      fontWeight: 'bold',
    },
    h5: {//heading6
      fontSize: 16,
      lineHeight: "19px",
      fontWeight: 'bold',
    },
    h6: {//heading7
      fontSize: 14,
      lineHeight: "17px",
      fontWeight: 'bold',
    },
    subtitle1: {//p2
      fontSize: 16,
      lineHeight: "19px",
      fontWeight: 'normal',
    },
    subtitle2: {//p3
      fontSize: 14,
      lineHeight: "17px",
      fontWeight: 'normal',
    },
    button: {
      textTransform: "none"
    }
  },
  palette: {
    primary: {
      main: "#FFFFFF",
    },
    secondary: {
      main: "#0000EE",
    },
  },
  overrides: {
    MuiButton: {
      contained: {
        borderRadius: '30px',
        marginRight: '8px',
        height: '36px',
      }
    }
  },
});

const useStyles = makeStyles({
  tableContainer: {
    display: 'flex',
    
  },
  table: {
    display: 'flex',
    [theme.breakpoints.between(500,700)]: {
      justifyContent:'center',
    },
    // justifyContent:'center',
  },
  toolbar: {
    padding: '0 0 0 0',
  },
  head: {
    padding: "60px 20px 60px 20px",
    [theme.breakpoints.down(780)]: {
      padding: "60px 11px 60px 11px",
    },
    [theme.breakpoints.down(700)]: {
      padding: "32px 11px 78px 11px",
    },
    [theme.breakpoints.down(400)]: {
      padding: "32px 4px 78px 4px",
    },
    [theme.breakpoints.down(360)]: {
      padding: "32px 0px 78px 0px",
    },
  },
  tableCell: {
    padding: "32px 0px 98px 0px",
    [theme.breakpoints.up(700)]: {
      padding: "60px 0px 60px 0px",
    },
  },
  cellHead: {
    textAlign: 'left',
    position: 'relative',
    bottom: '54px',
  },
  hidden: {
    WebkitUserSelect: 'none',
    KhtmlUserSelect: 'none',
    MozUserSelect: 'none',
    msUserSelect: 'none',
    userSelect: 'none',
    visibility: 'hidden',
  },
  img: {
    // display: 'flex',
    border: "1px solid #909090",
    boxSizing: "border-box",
    borderRadius: "12px",
    minWidth: '170px',
    width: "100%",
    maxWidth: '362px',
    minHeight: "112px",
    maxHeight: "240px",
    [theme.breakpoints.down(700)]: {
      maxWidth: '170px',
      maxHeight: "112px",
      minWidth: '0px',
      minHeight: "0px",
    },
  },
  nextItem: {
    height: "48px",
  },
  infosvg: {
    paddingRight: '8px', verticalAlign: 'top',
  },
  infoIcon: {
    position: 'absolute',
  },
  removeIcon: {
    padding: '0 0 0 0',
    position: 'relative',

    [theme.breakpoints.down(700)]: {
      top: '18px',
      left: '79px',
      // float: 'right',
    },
    [theme.breakpoints.up(700)]: {
      top: '20px',
      float: 'right',
    }
  }
});

const useToolbarStyles = makeStyles((theme) => ({
  grow: {
    flexGrow: 1,
    minWidth: '0px',
  },
  toolbar: {
    padding: '0 0 0 0',
  },
  exportIcon:{
    padding: '0 12px 0 12px',
    [theme.breakpoints.down(348)]: {
      padding: '0 6px 0 6px',
    },
  },
  shareIcon:{
    [theme.breakpoints.down(336)]: {
      padding: '0 6px 0 6px',
    },
  },
  likeIcon:{
    [theme.breakpoints.down(324)]: {
      padding: '0 6px 0 6px',
    },
  }
}));

TableToolbar.propTypes = {
  classes: PropTypes.object.isRequired,
  menuState: PropTypes.object.isRequired,
  setMenuState: PropTypes.func.isRequired,
};

function TableToolbar(props) {
  const classes = useToolbarStyles();
  const { menuState, setMenuState } = props;

  function handleFilter(event) {
    setMenuState(false);
    // eslint-disable-next-line
    const timer = setTimeout(() => (setMenuState(true)), 1);

    if (filterList.indexOf(event.target.textContent) === -1) {
      if (event.target.textContent === "Price")
        filterList.push(event.target.textContent, "Valuation");
      else if (event.target.textContent === "Develop. Info")
        filterList.push(event.target.textContent, "Developinfo");
      else if (event.target.textContent === "Local Info")
        filterList.push(event.target.textContent, "LocalInfo", "Schools", "Crime", "Shop&Eat");
      else
        filterList.push(event.target.textContent);
    }
    else {
      if (event.target.textContent === "Price" || event.target.textContent === "Develop. Info")
        filterList.splice(filterList.indexOf(event.target.textContent), 2);
      else if (event.target.textContent === "Local Info")
        filterList.splice(filterList.indexOf(event.target.textContent), 5);
      else
        filterList.splice(filterList.indexOf(event.target.textContent), 1);
    }
    // console.log(event, event.target.textContent, filterList);
  }

  return (
    <Box>
      <Box>
        <Toolbar
          className={classes.toolbar}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        >
          <Typography edge="start" variant="h1">Compare</Typography>
          <IconButton className={classes.menuIcon}
            onClick={() => {
              if (menuState)
                setMenuState(false);
              else
                setMenuState(true);
            }} >
            {menuState ? (
              <SvgIcon
                width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="32" height="32" rx="16" fill="#0000EE" />
                <path d="M11.2 8V16.5867C10.5972 16.7091 10.0552 17.0361 9.6659 17.5124C9.27662 17.9887 9.06396 18.5849 9.06396 19.2C9.06396 19.8151 9.27662 20.4113 9.6659 20.8876C10.0552 21.3639 10.5972 21.6909 11.2 21.8133V24H12.2666V21.8133C12.8695 21.6909 13.4114 21.3639 13.8007 20.8876C14.19 20.4113 14.4026 19.8151 14.4026 19.2C14.4026 18.5849 14.19 17.9887 13.8007 17.5124C13.4114 17.0361 12.8695 16.7091 12.2666 16.5867V8H11.2Z" fill="white" />
                <path d="M19.7332 8V10.1867C19.1304 10.3091 18.5884 10.6361 18.1991 11.1124C17.8098 11.5887 17.5972 12.1849 17.5972 12.8C17.5972 13.4151 17.8098 14.0113 18.1991 14.4876C18.5884 14.9639 19.1304 15.2909 19.7332 15.4133V24H20.7998V15.4133C21.4027 15.2909 21.9446 14.9639 22.3339 14.4876C22.7232 14.0113 22.9358 13.4151 22.9358 12.8C22.9358 12.1849 22.7232 11.5887 22.3339 11.1124C21.9446 10.6361 21.4027 10.3091 20.7998 10.1867V8H19.7332Z" fill="white" />
              </SvgIcon>) : (<SvgIcon
                width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect x="0.5" y="0.5" width="31" height="31" rx="15.5" fill="white" stroke="#222222" />
                <path d="M11.2 8V16.5867C10.5972 16.7091 10.0552 17.0361 9.6659 17.5124C9.27662 17.9887 9.06396 18.5849 9.06396 19.2C9.06396 19.8151 9.27662 20.4113 9.6659 20.8876C10.0552 21.3639 10.5972 21.6909 11.2 21.8133V24H12.2666V21.8133C12.8695 21.6909 13.4114 21.3639 13.8007 20.8876C14.19 20.4113 14.4026 19.8151 14.4026 19.2C14.4026 18.5849 14.19 17.9887 13.8007 17.5124C13.4114 17.0361 12.8695 16.7091 12.2666 16.5867V8H11.2Z" fill="#222222" />
                <path d="M19.7332 8V10.1867C19.1304 10.3091 18.5884 10.6361 18.1991 11.1124C17.8098 11.5887 17.5972 12.1849 17.5972 12.8C17.5972 13.4151 17.8098 14.0113 18.1991 14.4876C18.5884 14.9639 19.1304 15.2909 19.7332 15.4133V24H20.7998V15.4133C21.4027 15.2909 21.9446 14.9639 22.3339 14.4876C22.7232 14.0113 22.9358 13.4151 22.9358 12.8C22.9358 12.1849 22.7232 11.5887 22.3339 11.1124C21.9446 10.6361 21.4027 10.3091 20.7998 10.1867V8H19.7332Z" fill="#222222" />
              </SvgIcon>)}
          </IconButton>
          <div className={classes.grow}></div>
          <div className={classes.iconButtonSet} >
            <IconButton className={classes.exportIcon}>
              <SvgIcon
                onClick={() => {
                  console.log("export");
                }}
                width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M15 1.25L10 6.25H13.75V17.5H16.25V6.25H20L15 1.25ZM22.5 28.75H7.5C6.83696 28.75 6.20107 28.4866 5.73223 28.0178C5.26339 27.5489 5 26.913 5 26.25V11.25C5 10.587 5.26339 9.95107 5.73223 9.48223C6.20107 9.01339 6.83696 8.75 7.5 8.75H11.25V11.25H7.5V26.25H22.5V11.25H18.75V8.75H22.5C23.163 8.75 23.7989 9.01339 24.2678 9.48223C24.7366 9.95107 25 10.587 25 11.25V26.25C25 26.913 24.7366 27.5489 24.2678 28.0178C23.7989 28.4866 23.163 28.75 22.5 28.75Z" fill="black" />
              </SvgIcon>
            </IconButton>
            <IconButton className={classes.shareIcon}>
              <SvgIcon
                onClick={() => {
                  console.log("share");
                }}
                width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M22.5 20.1C21.55 20.1 20.7 20.475 20.05 21.0625L11.1375 15.875C11.2 15.5875 11.25 15.3 11.25 15C11.25 14.7 11.2 14.4125 11.1375 14.125L19.95 8.9875C20.625 9.6125 21.5125 10 22.5 10C24.575 10 26.25 8.325 26.25 6.25C26.25 4.175 24.575 2.5 22.5 2.5C20.425 2.5 18.75 4.175 18.75 6.25C18.75 6.55 18.8 6.8375 18.8625 7.125L10.05 12.2625C9.375 11.6375 8.4875 11.25 7.5 11.25C5.425 11.25 3.75 12.925 3.75 15C3.75 17.075 5.425 18.75 7.5 18.75C8.4875 18.75 9.375 18.3625 10.05 17.7375L18.95 22.925C18.8875 23.1875 18.85 23.4625 18.85 23.75C18.85 25.7625 20.4875 27.3875 22.5 27.3875C24.5125 27.3875 26.15 25.7625 26.15 23.75C26.15 21.7375 24.5125 20.1 22.5 20.1ZM22.5 5C23.1875 5 23.75 5.5625 23.75 6.25C23.75 6.9375 23.1875 7.5 22.5 7.5C21.8125 7.5 21.25 6.9375 21.25 6.25C21.25 5.5625 21.8125 5 22.5 5ZM7.5 16.25C6.8125 16.25 6.25 15.6875 6.25 15C6.25 14.3125 6.8125 13.75 7.5 13.75C8.1875 13.75 8.75 14.3125 8.75 15C8.75 15.6875 8.1875 16.25 7.5 16.25ZM22.5 25C21.8125 25 21.25 24.4375 21.25 23.75C21.25 23.0625 21.8125 22.5 22.5 22.5C23.1875 22.5 23.75 23.0625 23.75 23.75C23.75 24.4375 23.1875 25 22.5 25Z" fill="black" />
              </SvgIcon>
            </IconButton>
            <IconButton
              className={classes.likeIcon}
              edge="end"
            >
              <SvgIcon onClick={() => {
                console.log("like");
              }}
                width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M15.125 23.1875L15 23.3125L14.8625 23.1875C8.925 17.8 5 14.2375 5 10.625C5 8.125 6.875 6.25 9.375 6.25C11.3 6.25 13.175 7.5 13.8375 9.2H16.1625C16.825 7.5 18.7 6.25 20.625 6.25C23.125 6.25 25 8.125 25 10.625C25 14.2375 21.075 17.8 15.125 23.1875ZM20.625 3.75C18.45 3.75 16.3625 4.7625 15 6.35C13.6375 4.7625 11.55 3.75 9.375 3.75C5.525 3.75 2.5 6.7625 2.5 10.625C2.5 15.3375 6.75 19.2 13.1875 25.0375L15 26.6875L16.8125 25.0375C23.25 19.2 27.5 15.3375 27.5 10.625C27.5 6.7625 24.475 3.75 20.625 3.75Z" fill="black" />
              </SvgIcon>
            </IconButton>
          </div>
          <br></br>
        </Toolbar>
      </Box>
      <Box>
        {menuState ? (
          <Toolbar
            style={{ overflow: 'auto' }}
            className={classes.toolbar}
            anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
            transformOrigin={{ vertical: 'top', horizontal: 'right' }}
          >
            <Button variant="contained"
              className={classes.button}
              color={filterList.indexOf("Size") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>
              <Typography variant='h6'>Size</Typography>
            </Button>
            <Button variant="contained"
              value="Price"
              color={filterList.indexOf("Price") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>
              <Typography variant='h6'>Price</Typography>
            </Button>
            <Button variant="contained"
              value="Rent"
              color={filterList.indexOf("Rent") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>
              <Typography variant='h6'>Rent</Typography>
            </Button>
            <Button variant="contained"
              value="Layout"
              color={filterList.indexOf("Layout") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>

              <Typography variant='h6'>Layout</Typography>
            </Button>
            <Button variant="contained"
              value="Develop. Info"
              color={filterList.indexOf("Develop. Info") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>

              <Typography variant='h6'>Develop. Info</Typography>
            </Button>
            <Button variant="contained"
              value="Amenities"
              color={filterList.indexOf("Amenities") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>
              <Typography variant='h6'>Amenities</Typography>

            </Button>
            <Button variant="contained"
              value="Local Info"
              color={filterList.indexOf("Local Info") !== -1 ? "primary" : "secondary"}
              onClick={(event) => { handleFilter(event) }}>
              <Typography variant='h6'>Local Info</Typography>
            </Button>
          </Toolbar>) : (null)}
      </Box>
    </Box>
  );
}

export default function TheTable() {
  const classes = useStyles();
  const [menuState, setMenuState] = React.useState(false);

  function NextItem() {
    return (
      <div style={{ height: '48px' }} />
    );
  }
  function headerOutput(pn) {
    if (pn === "Schools")
      return <Typography variant='h5'>{pn}</Typography>;
    switch (pn) {
      case "Developinfo":
        return <Typography variant='h3'>{"Develop. Info"}</Typography>;
      case "LocalInfo":
        return (
          <div>
            <Typography variant='h3'>{"Local Info"}</Typography>
            <Typography variant='h5'>{"Transport."}</Typography>
          </div>
        );
      case "Shop&Eat":
        return (<div>
          <Typography variant='h3'>{"Shop & Eat"}</Typography>
          <Typography variant='subtitle2'>{"(4 stars or above)"}</Typography></div>);
      case "Crime":
        return (
          <Typography variant='h3'>{pn}<img src='https://svgshare.com/i/Z0j.svg' title='' alt=''
            onClick={() => {
              console.log("Crime");
            }} /></Typography>
        );
      case "Valuation":
        return (
          <Typography variant='h3'>{pn}<img src='https://svgshare.com/i/Z0j.svg' title='' alt=''
            onClick={() => {
              console.log("Valuation");
            }} /></Typography>
        );
      default:
        return <Typography variant='h3'>{pn}</Typography>;
    }
  }
  function mHeaderOutput(pn) {
    if (pn === "Schools")
      return <Typography variant='h6'>{pn}</Typography>;
    switch (pn) {
      case "Developinfo":
        return <Typography variant='h4'>{"Develop. Info"}</Typography>;
      case "LocalInfo":
        return (
          <div>
            <Typography variant='h4'>{"Local Info"}</Typography>
            <Typography variant='h6'>{"Transport."}</Typography>
          </div>
        );
      case "Shop&Eat":
        return (<div>
          <Typography variant='h6'>{"Shop & Eat"}</Typography>
          <Typography style={{
            fontSize: 12,
            lineHeight: '15px',
            fontWeight: 'normal',
          }}>{"(4 stars or above)"}</Typography></div>);
      case "Crime":
        return (
          <Typography variant='h6'>{pn}<img src='https://svgshare.com/i/Z0j.svg' title='' alt=''
            onClick={() => {
              console.log("Crime");
            }} /></Typography>
        );
      case "Valuation":
        return (
          <Typography variant='h4'>{pn}<img src='https://svgshare.com/i/Z0j.svg' title='' alt=''
            onClick={() => {
              console.log("Valuation");
            }} /></Typography>
        );
      default:
        return <Typography variant='h4'>{pn}</Typography>;
    }
  }

  function cellOutput(no, pn) {
    switch (pn) {
      case "Valuation":
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography variant='h1'>{rows[no][pn]["price"]}</Typography>
          <Typography variant='subtitle1'>{rows[no][pn]["date"]}</Typography>
        </TableCell>);
      case "Layout":
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography variant='h1'>{rows[no][pn]["Bedroom"]} Bedroom</Typography>
        </TableCell>);
      case "Developinfo":
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography variant='h2'>No. {rows[no][pn]["TotalUnitofdevelopment"]}</Typography>
          <Typography variant='subtitle1'>Total Unit of development</Typography>
          <NextItem />
          <Typography variant='h2'>
            <img src='https://svgshare.com/i/Z2L.svg' alt="" className={classes.infosvg} />
            {rows[no][pn]["yrsSince1994"]}</Typography>
          <Typography variant='subtitle1'>yrs Since 1994</Typography>
          <NextItem />
          <Typography variant='h2'>{rows[no][pn]["Developer"]}</Typography>
          <Typography variant='subtitle1'>Developer</Typography>
        </TableCell>);
      case "Amenities":
        return (
          <TableCell align="center" className={classes.tableCell}>
            {amenitiesOutput(rows[no][pn]["Clubhouse"], "Clubhouse")}
            {amenitiesOutput(rows[no][pn]["Pool"], "Pool")}
            {amenitiesOutput(rows[no][pn]["Kids"], "Kids")}
            {amenitiesOutput(rows[no][pn]["Sports"], "Sports")}
          </TableCell>);
      case "LocalInfo":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography variant='h2'>{rows[no][pn]["Transport"]["CityCentre"]["time"]}</Typography>
            <Typography variant='subtitle1'>{rows[no][pn]["Transport"]["CityCentre"]["name"]}</Typography>
            <NextItem />
            <Typography variant='h2'>
              <img src='https://svgshare.com/i/Z2R.svg' alt="" className={classes.infosvg} />
              {rows[no][pn]["Transport"]["Metro"]["station"]}</Typography>
            <Typography variant='subtitle1'>{rows[no][pn]["Transport"]["Metro"]["walk"]} walk</Typography>
            <NextItem />
            <Typography variant='h2'><img src='https://svgshare.com/i/Z1d.svg' alt="" className={classes.infosvg} />{rows[no][pn]["Transport"]["Bus"]["Lines"]} Lines</Typography>
            <Typography variant='subtitle1'>{rows[no][pn]["Transport"]["Bus"]["name"]}</Typography>
          </TableCell>);
      case "Schools":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography variant='h2'>{rows[no][pn]["SchoolNetwork"]}</Typography>
            <Typography variant='subtitle2'>School Network</Typography>
            <NextItem />
            <Typography variant='h2'>{rows[no][pn]["Postcode/District"]}</Typography>
            <Typography variant='subtitle2'>Post code / District</Typography>
            <NextItem />
            <Typography variant='h2'>{rows[no][pn]["State"]}</Typography>
            <Typography variant='subtitle2'>State School</Typography>
            <NextItem />
            <Typography variant='h2'>{rows[no][pn]["Inde"]}</Typography>
            <Typography variant='subtitle2'>Inde. School</Typography>
            <NextItem />
            <Typography variant='h5'>Top 3</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3"]["n1"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3"]["n2"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3"]["n3"]}</Typography>
          </TableCell >);
      case "Crime":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography variant='h2'>Grade {rows[no][pn]["SafetyLevel"]}</Typography>
            <Typography variant='subtitle2'>Safety Level</Typography>
          </TableCell>);
      case "Shop&Eat":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography variant='h2'>{rows[no][pn]["Restaurants/Cafe"]}</Typography>
            <Typography variant='subtitle2'>Restaurants & cafes nearby</Typography>
            <NextItem />
            <Typography variant='h2'>{rows[no][pn]["Supermarket/shoppingmall"]}</Typography>
            <Typography variant='subtitle2'>Supermarket & shopping mall nearby</Typography>
            <NextItem />
            <Typography variant='h2'>{rows[no][pn]["Nightlife/Arts/Entertainment"]}</Typography>
            <Typography variant='subtitle2'>Night life & Arts & Entert.</Typography>
            <NextItem />
            <Typography variant='h5'>Top 3 Hot Spots</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3HotSpots"]["n1"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3HotSpots"]["n2"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3HotSpots"]["n3"]}</Typography>
          </TableCell>
        );
      default:
        return (<TableCell align="center" className={classes.tableCell}><Typography variant='h1'>{rows[no][pn]}</Typography></TableCell>);
    }
  };

  function mCellOutput(no, pn) {
    switch (pn) {
      case "Valuation":
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
          <Typography variant='h3'>{rows[no][pn]["price"]}</Typography>
          <Typography variant='subtitle1'>{rows[no][pn]["date"]}</Typography>
        </TableCell>);
      case "Layout":
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
          <Typography variant='h3'>{rows[no][pn]["Bedroom"]} Bedroom</Typography>
        </TableCell>);
      case "Developinfo":
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
          <Typography variant='h3'>No. {rows[no][pn]["TotalUnitofdevelopment"]}</Typography>
          <Typography variant='subtitle2'>Total Unit of development</Typography>
          <NextItem />
          <Typography variant='h3'>
            <img src='https://svgshare.com/i/Z2L.svg' alt="" className={classes.infosvg} />
            {rows[no][pn]["yrsSince1994"]}</Typography>
          <Typography variant='subtitle2'>yrs Since 1994</Typography>
          <NextItem />
          <Typography variant='h3'>{rows[no][pn]["Developer"]}</Typography>
          <Typography variant='subtitle2'>Developer</Typography>
        </TableCell>);
      case "Amenities":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
            {amenitiesOutput(rows[no][pn]["Clubhouse"], "Clubhouse")}
            {amenitiesOutput(rows[no][pn]["Pool"], "Pool")}
            {amenitiesOutput(rows[no][pn]["Kids"], "Kids")}
            {amenitiesOutput(rows[no][pn]["Sports"], "Sports")}
          </TableCell>);
      case "LocalInfo":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography style={{ bottom: '73px', }} className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
            <Typography variant='h3'>{rows[no][pn]["Transport"]["CityCentre"]["time"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Transport"]["CityCentre"]["name"]}</Typography>
            <NextItem />
            <Typography variant='h3'>
              <img src='https://svgshare.com/i/Z2R.svg' alt="" className={classes.infosvg} />
              {rows[no][pn]["Transport"]["Metro"]["station"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Transport"]["Metro"]["walk"]} walk</Typography>
            <NextItem />
            <Typography variant='h3'><img src='https://svgshare.com/i/Z1d.svg' alt="" className={classes.infosvg} />{rows[no][pn]["Transport"]["Bus"]["Lines"]} Lines</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Transport"]["Bus"]["name"]}</Typography>
          </TableCell>);
      case "Schools":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
            <Typography variant='h3'>{rows[no][pn]["SchoolNetwork"]}</Typography>
            <Typography variant='subtitle2'>School Network</Typography>
            <NextItem />
            <Typography variant='h3'>{rows[no][pn]["Postcode/District"]}</Typography>
            <Typography variant='subtitle2'>Post code / District</Typography>
            <NextItem />
            <Typography variant='h3'>{rows[no][pn]["State"]}</Typography>
            <Typography variant='subtitle2'>State School</Typography>
            <NextItem />
            <Typography variant='h3'>{rows[no][pn]["Inde"]}</Typography>
            <Typography variant='subtitle2'>Inde. School</Typography>
            <NextItem />
            <Typography variant='h5'>Top 3</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3"]["n1"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3"]["n2"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3"]["n3"]}</Typography>
          </TableCell >);
      case "Crime":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
            <Typography variant='h3'>Grade {rows[no][pn]["SafetyLevel"]}</Typography>
            <Typography variant='subtitle2'>Safety Level</Typography>
          </TableCell>);
      case "Shop&Eat":
        return (
          <TableCell align="center" className={classes.tableCell}>
            <Typography variant='h6' style={{ bottom: '69px' }} className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
            <Typography variant='h3'>{rows[no][pn]["Restaurants/Cafe"]}</Typography>
            <Typography variant='subtitle2'>Restaurants & cafes nearby</Typography>
            <NextItem />
            <Typography variant='h3'>{rows[no][pn]["Supermarket/shoppingmall"]}</Typography>
            <Typography variant='subtitle2'>Supermarket & shopping mall nearby</Typography>
            <NextItem />
            <Typography variant='h3'>{rows[no][pn]["Nightlife/Arts/Entertainment"]}</Typography>
            <Typography variant='subtitle2'>Night life & Arts & Entert.</Typography>
            <NextItem />
            <Typography variant='h5'>Top 3 Hot Spots</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3HotSpots"]["n1"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3HotSpots"]["n2"]}</Typography>
            <Typography variant='subtitle2'>{rows[no][pn]["Top3HotSpots"]["n3"]}</Typography>
          </TableCell>
        );
      default:
        return (<TableCell align="center" className={classes.tableCell}>
          <Typography className={no === 0 ? classes.cellHead : classes.hidden}>{mHeaderOutput(pn)}</Typography>
          <Typography variant='h3'>{rows[no][pn]}</Typography>
        </TableCell>);
    }
  };

  function amenitiesOutput(a, aType) {
    if (a) {
      switch (aType) {
        case "Clubhouse":
          return (<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" style={{ paddingRight: "20px" }}>
            <path d="M15 6.25C13.6125 6.25 12.5 7.3625 12.5 8.75C12.5 10.1375 13.6125 11.25 15 11.25C16.3875 11.25 17.5 10.1375 17.5 8.75C17.5 7.3625 16.3875 6.25 15 6.25ZM27.5 1.25V7.5H25V5H5V7.5H2.5V1.25H5V3.75H25V1.25H27.5ZM18.75 14.075V28.75H16.25V22.5H13.75V28.75H11.25V14.075C8.6625 12.7125 6.875 10 6.875 6.875V6.25H9.375V6.875C9.375 10 11.875 12.5 15 12.5C18.125 12.5 20.625 10 20.625 6.875V6.25H23.125V6.875C23.125 10 21.3375 12.7125 18.75 14.075Z" fill="#222222" />
          </svg>
          );
        case "Pool":
          return (<svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" style={{ paddingRight: "20px" }}>
            <path d="M2.5 22.5C5.275 21.25 8.05 20 10.8375 20C13.6125 20 16.3875 22.5 19.1625 22.5C21.95 22.5 24.725 20 27.5 20V23.75C24.725 23.75 21.95 26.25 19.1625 26.25C16.3875 26.25 13.6125 23.75 10.8375 23.75C8.05 23.75 5.275 25 2.5 26.25V22.5ZM10.8375 16.25C9.8625 16.25 8.9 16.4 7.9375 16.65L14.0875 12.35L12.7875 10.8C12.6125 10.5875 12.5 10.3 12.5 10C12.5 9.57502 12.7125 9.18752 13.05 8.96252L20.2 3.96252L21.6375 6.00002L15.5875 10.2375L22.125 18.025C21.1375 18.4375 20.15 18.75 19.1625 18.75C16.3875 18.75 13.6125 16.25 10.8375 16.25ZM22.5 8.75002C23.163 8.75002 23.7989 9.01342 24.2678 9.48226C24.7366 9.9511 25 10.587 25 11.25C25 11.9131 24.7366 12.549 24.2678 13.0178C23.7989 13.4866 23.163 13.75 22.5 13.75C21.837 13.75 21.2011 13.4866 20.7322 13.0178C20.2634 12.549 20 11.9131 20 11.25C20 10.587 20.2634 9.9511 20.7322 9.48226C21.2011 9.01342 21.837 8.75002 22.5 8.75002Z" fill="black" />
          </svg>
          );
        case "Kids":
          return (
            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" style={{ paddingRight: "20px" }}>
              <path d="M8.75 2.5C7.375 2.5 6.25 3.625 6.25 5C6.25 6.375 7.375 7.5 8.75 7.5C10.125 7.5 11.25 6.3875 11.25 5C11.25 3.6125 10.1375 2.5 8.75 2.5ZM6.25 8.75C4.8625 8.75 3.75 9.8625 3.75 11.25V18.75H6.25V27.5H12.5V14.5L15.6625 20H18.7125L20 18.325V27.5H25V21.25H26.25V17.5C26.25 16.1125 25.1375 15 23.75 15H20.625C19.875 15 19.2125 15.325 18.75 15.85C18.3375 16.375 17.9 16.95 17.5 17.5H17.1125L12.5 9.575C12.3 9.225 11.525 8.75 10.625 8.75H6.25ZM22.5 10C21.4625 10 20.625 10.8375 20.625 11.875C20.625 12.9125 21.4625 13.75 22.5 13.75C23.5375 13.75 24.375 12.9125 24.375 11.875C24.375 10.8375 23.5375 10 22.5 10Z" fill="black" />
            </svg>
          );
        case "Sports":
          return (
            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
              <g clipPath="url(#clip0)">
                <path d="M25.8252 21.25C29.2752 15.2744 27.2255 7.62468 21.2499 4.17468C15.2743 0.724682 7.62456 2.77442 4.17456 8.75C0.72456 14.7256 2.7743 22.3753 8.74988 25.8253C14.7255 29.2753 22.3752 27.2256 25.8252 21.25ZM23.8319 10.3275C22.3463 10.7256 20.7589 10.6751 19.2637 10.1149L21.0387 7.04052C22.1945 7.9234 23.1471 9.04437 23.8319 10.3275ZM18.8736 5.79052L17.0986 8.86491C15.8933 7.87143 15.0301 6.52493 14.6304 5.01501C16.084 4.96651 17.5311 5.23099 18.8736 5.79052ZM7.0404 8.96122C8.35981 7.22594 10.1792 6.02469 12.1633 5.43815C12.7001 7.67837 13.9983 9.6626 15.8361 11.0516L14.5423 13.2925L7.0404 8.96122ZM16.7074 14.5425L18.0012 12.3016C20.1295 13.1973 22.5024 13.3279 24.7161 12.6711C25.2081 14.7401 25.0307 16.912 24.2094 18.8737L16.7074 14.5425ZM11.1262 24.2095L12.9012 21.1351C14.1339 22.1499 14.9713 23.4994 15.3694 24.985C13.9443 25.0283 12.4973 24.7846 11.1262 24.2095ZM22.9594 21.0388C21.6399 22.7741 19.8314 23.9816 17.8411 24.5789C17.303 22.3334 16.0035 20.3437 14.1637 18.9484L15.4574 16.7075L22.9594 21.0388ZM5.7904 11.1263L13.2923 15.4575L11.9986 17.6984C9.87551 16.8068 7.50992 16.6747 5.30077 17.3243C4.8002 15.2589 4.97173 13.0875 5.7904 11.1263ZM6.16787 19.6725C7.67565 19.2664 9.27255 19.3407 10.7361 19.8851L8.96109 22.9595C7.80527 22.0766 6.85268 20.9556 6.16787 19.6725Z" fill="black" />
              </g>
              <defs>
                <clipPath id="clip0">
                  <rect width="30" height="30" fill="white" />
                </clipPath>
              </defs>
            </svg>);
        default:
          return;
      }
    }
  }

  if (useMediaQuery(theme.breakpoints.up(700))) {
    return (
      <ThemeProvider theme={theme}>
        <Box display="flex" p={1}>
          <Box p={0} flexShrink={1} width="292px" />
          <Box p={1} width="100%">
            <TableToolbar
              menuState={menuState}
              setMenuState={setMenuState}
            />
            <TableContainer className={classes.tableContainer}>
              <Table
                className={classes.table}
                aria-label="simple table"
              >
                <TableBody>
                  <TableRow>
                    <p></p>
                    <TableCell align="center" className={classes.head}>
                      <svg width="24" height="24" viewBox="20 16 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={classes.removeIcon}
                        onClick={() => {
                          console.log("remove1");
                        }}>
                        <g filter="url(#filter0_d)">
                          <circle cx="32" cy="28" r="11.5" fill="white" stroke="#222222" />
                          <path d="M26 26.8H38V29.2H26V26.8Z" fill="#222222" />
                        </g>
                        <defs>
                          <filter id="filter0_d" x="0" y="0" width="64" height="64" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                            <feOffset dy="4" />
                            <feGaussianBlur stdDeviation="10" />
                            <feColorMatrix type="matrix" values="0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0.2 0" />
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                          </filter>
                        </defs>
                      </svg>
                      <br></br>
                      <img src={rows[0][name[0]]} className={classes.img} alt=''></img>
                      <br></br>
                      <br></br>
                      <Typography variant='h3'>{rows[0][name[1]]}</Typography>
                    </TableCell>
                    <TableCell align="center" className={classes.head}>
                      <svg width="24" height="24" viewBox="20 16 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={classes.removeIcon}
                        onClick={() => {
                          console.log("remove2");
                        }}>
                        <g filter="url(#filter0_d)">
                          <circle cx="32" cy="28" r="11.5" fill="white" stroke="#222222" />
                          <path d="M26 26.8H38V29.2H26V26.8Z" fill="#222222" />
                        </g>
                        <defs>
                          <filter id="filter0_d" x="0" y="0" width="64" height="64" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                            <feOffset dy="4" />
                            <feGaussianBlur stdDeviation="10" />
                            <feColorMatrix type="matrix" values="0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0.2 0" />
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                          </filter>
                        </defs>
                      </svg>
                      <br></br>
                      <img src={rows[1][name[0]]} className={classes.img} alt=''></img>
                      <br></br>
                      <br></br>
                      <Typography variant='h3'>{rows[1][name[1]]}</Typography>
                    </TableCell>
                    <TableCell align="center" className={classes.head}>
                      <svg width="24" height="24" viewBox="20 16 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={classes.removeIcon}
                        onClick={() => {
                          console.log("remove3");
                        }}>
                        <g filter="url(#filter0_d)">
                          <circle cx="32" cy="28" r="11.5" fill="white" stroke="#222222" />
                          <path d="M26 26.8H38V29.2H26V26.8Z" fill="#222222" />
                        </g>
                        <defs>
                          <filter id="filter0_d" x="0" y="0" width="64" height="64" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                            <feOffset dy="4" />
                            <feGaussianBlur stdDeviation="10" />
                            <feColorMatrix type="matrix" values="0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0.2 0" />
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                          </filter>
                        </defs>
                      </svg>
                      <br></br>
                      <img src={rows[2][name[0]]} className={classes.img} alt=''></img>
                      <br></br>
                      <br></br>
                      <Typography variant='h3'>{rows[2][name[1]]}</Typography>
                    </TableCell>
                  </TableRow>
                  {
                    propertyNames.map((pn) => {
                      if (filterList.indexOf(pn) === -1)
                        return (
                          <TableRow>
                            <Typography style={{ position: "relative", top: "61px" }}>{headerOutput(pn)}</Typography>
                            {cellOutput(0, pn)}
                            {cellOutput(1, pn)}
                            {cellOutput(2, pn)}
                          </TableRow>
                        )
                      else
                        return null;
                    })
                  }
                </TableBody>
              </Table>
            </TableContainer>
          </Box>
          <Box p={0} flexShrink={1} width="277px" />
        </Box>
      </ThemeProvider >
    );
  } else {
    return (
      <ThemeProvider theme={theme}>
        <Box display="flex" p={1}>

          <Box p={0} width="100%">
            <TableToolbar
              menuState={menuState}
              setMenuState={setMenuState}
            />
            <TableContainer className={classes.tableContainer}>
              <Table
                className={classes.table}
                aria-label="simple table"
              >
                <TableBody>
                  <TableRow >
                    <TableCell align="center" className={classes.head}>
                      <svg width="22" height="22" viewBox="20 16 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={classes.removeIcon}
                        onClick={() => {
                          console.log("remove1");
                        }}>
                        <g filter="url(#filter0_d)">
                          <circle cx="32" cy="28" r="11.5" fill="white" stroke="#222222" />
                          <path d="M26 26.8H38V29.2H26V26.8Z" fill="#222222" />
                        </g>
                        <defs>
                          <filter id="filter0_d" x="0" y="0" width="64" height="64" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                            <feOffset dy="4" />
                            <feGaussianBlur stdDeviation="10" />
                            <feColorMatrix type="matrix" values="0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0.2 0" />
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                          </filter>
                        </defs>
                      </svg>
                      <br></br>
                      <img src={rows[0][name[0]]} className={classes.img} alt=''></img>
                      <br></br>
                      <br></br>
                      <Typography variant='h6' >{rows[0][name[1]]}</Typography>
                    </TableCell>
                    <TableCell align="center" className={classes.head}>
                      <svg width="22" height="22" viewBox="20 16 24 24" fill="none" xmlns="http://www.w3.org/2000/svg" className={classes.removeIcon}
                        onClick={() => {
                          console.log("remove2");
                        }}>
                        <g filter="url(#filter0_d)">
                          <circle cx="32" cy="28" r="11.5" fill="white" stroke="#222222" />
                          <path d="M26 26.8H38V29.2H26V26.8Z" fill="#222222" />
                        </g>
                        <defs>
                          <filter id="filter0_d" x="0" y="0" width="64" height="64" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                            <feFlood flood-opacity="0" result="BackgroundImageFix" />
                            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
                            <feOffset dy="4" />
                            <feGaussianBlur stdDeviation="10" />
                            <feColorMatrix type="matrix" values="0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0 0.133333 0 0 0 0.2 0" />
                            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
                            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
                          </filter>
                        </defs>
                      </svg>
                      <br></br>
                      <img src={rows[1][name[0]]} className={classes.img} alt=''></img>
                      <br></br>
                      <br></br>
                      <Typography variant='h6' >{rows[1][name[1]]}</Typography>
                    </TableCell>
                  </TableRow>
                  {
                    propertyNames.map((pn) => {
                      if (filterList.indexOf(pn) === -1)
                        return (
                          <TableRow>
                            {mCellOutput(0, pn)}
                            {mCellOutput(1, pn)}
                          </TableRow>
                        )
                      else
                        return null;
                    })
                  }
                </TableBody>
              </Table>
            </TableContainer>
          </Box>

        </Box>
      </ThemeProvider >
    );
  }

}